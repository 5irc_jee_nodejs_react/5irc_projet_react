# A propos de React :
C'est une Librairie JavaScript (ES6) mais aussi un framework.
La déclinaison React Native permet de développer pour mobile (Android ou IOS).

De projet sous licence MIT est porté par Facebook, contenait lus de 20000 modules npm en 2017. De nombreux grands acteurs du web utilisent cette technologie pour leur front.


Elle joue les 3 rôles d'un front en tant qu'interface de programmation externe mélant :
- Structure (html)
- Style (css)
- Comportement (js)

React réunis dans un même fichier source ces trois volets et utilise une syntaxe concise : __JSX__
Ses interfaces sont une arborescence de __composants__ qui sont réutilisables.
Chaque composant peut en combiner d'autres, c'est la notion de __composition__.


## L'état applicatif
"Les données descendent, l'état remonte".
Lorsque les données évoluent, l'interface est censée réagir pour refléter le nouvel état.
ex: l'absence de données doit interdire l'accès d'actions.
Chaque composant est responsable des infos qu'il fournit a ses fils. Ces derniers peuvent signaler l'évolution de leur état à leurs parents.
Les mécanismes employés sont :
- __les props__
- __l'état local__


## Le DOM virtuel
React ne manipule pas directement le DOM du navigateur. Il décrit un DOM virtuel et réconciliera ce dernier avec  celui du navigateur.
Il permet de meilleurs performances.



## CRA : Create React app
C'est un outil écrit en Node6+ qui facilite le developpement d'apps fondées sur React.
Il génère un squelette applicatif qui contient chaque brique associée (JS ES2015+ "ES6", webpack, serveur..).
Pour générer un projet, __node/npm de version 6 minimum__ doit être installé :
Installer l'outil: `npm install --global create-react-app`

Puis créer le squelette:  `create-react-app 5irc_react`

Un fois ce dossier crée, à l'intérieur, plusieurs commandes sont possible:
__npm start__, npm build, npm test, npm eject ...

Le dossier node_modules contient les dépendances
Le dossier principal de travail est : src/ et il contient :
- index.js (le point d'entrée)
- __App.js (le premier composant)__ ce fichier n'est pas obligatoire
- index.html (une page support)


## Une classe React :
```
  class Screen extends Component {
    constructor (props) {
      super(props)
      this.state = { loginState: 'logged-out' }
    }
    render () {
      // …
    }
  }
```
- class : définit le corps de la classe
- extends : l'héritage
- constructor : définit le constructeur
- super : appel le constructeur hérité, il est obligatoire si elle utilise extends et si on utilise "this"

## les méthodes
  `nom (paramètres) { … }`

## les fonctions fléchées  =>


  ```const adults = [], minors = []
    people.forEach((person) => {
      if (person.age >= 18) {
        adults.push(person)
        } else {
        minors.push(person)
      }
    })

    people.map((person) => person.firstName)
    ```


Cette syntaxe est pratique pour les __prédicats__ (Booléen, ou passées aux méthodes filter, every, some...).
Et pour les __mappers__ (map)   Elles seront utiles pour réduire le bruit dans les méthodes render.

## this
Au sein d'une fonction JS, __this__ était local.
Les fonctions fléchées ne redéfinissent aucun identifiant de type __this__


```
const name = 'Extérieur'
const obj = {
  name: 'Intérieur',
  runGreet () {
    // Ici, this.name est bien "Intérieur"
    setTimeout(() => {
      // Ici dans la fonction fléchée, this n’est pas redéfini par la fonction,
      // comme n’importe quel identifiant, il est donc recherché dans les
      // portées englobantes, et trouvé au niveau de runGreet, c’est donc aussi "Intérieur".
    }, 0)
  }
}
obj.runGreet()
```

## Déstructuration

La destructuration permet d'aller chercher rapidement plusieurs propriétés au sein d'un objet ou de plusieurs cellules d'un objet itérable de type tableau.

### Destructuration sur des propriétés
Avant :
```
const firstName = this.props.firstName
const lastName = this.props.lastName
const onClick = this.props.onClick
```
Avec une déstructuration basée sur les noms: `const { firstName, lastName, onClick } = this.props`


### Destructuration d'un tableau
Avant :
```
const names = fullName.split(' ')
const firstName = names[0]
const lastName = names[1]
```
Avec une déstructuration basée sur les positions: `const [firstName, lastName] = fullName.split(' ')`



## Modules natifs, imports et exports
L'application est découpée en modules, qui sont autant de fichiers sources séparés.

Pour rendre visible un module :   `export`

Pour utiliser un module :   `import`



Exemples :
Au sein du fichier textUtils.js:
```
export function countWords (text) {
  return text.split(/\W+/u).filter(Boolean).length
}

export function normalizeSpacing (text) {
  return text.replace(/\s+/u, ' ').trim()
}
```


Au sein d’un fichier main.js, dans le même répertoire :
```
import { countWords } from './textUtils'
console.log(countWords('Hello world, this is nice!'))  
```

Export par défaut :
Dans le module exportateur, SuperComponent.js :
  `export default class SuperComponent { … }`



Dans le module importateur, dans le même répertoire :
  `import GreatComponent from './SuperComponent'`




## Écriture de fonctions pures avec JSX
On peut définir un composant React à l'aide d'une simple fonction (composant pur fonctionnel). Il renvoi toujours la même chose pour les mêmes arguments (plus facile à tester).
Ces composant représentent souvent près de 90% d'une application.

exemple de composant retournant un DOM Virtuel à l'aide de __React.createElement()__ :
```
function CoolComponent() {
  return React.createElement('p', {}, 'Youpi So Cool !')
}
```
- le 1er argument est le nom du composant (ici une string)
- le 2nd argument est une série d'attributs (id, classname...)
- le 3eme est le contenu (ici du texte)


Pour afficher un DOM virtuel on utilise __ReactDOM.render(...)__ (comme dans src/index.js):
```
ReactDOM.render(
  React.createElement(CoolComponent),
  document.getElementById('root')
)
```
Avec la syntaxe __<span style="color:yellow">JSX</span>__ :
```
function CoolComponent() {
  return <p>Youpi So Cool !</p>
}

ReactDOM.render(
  <CoolComponent />,
  document.getElementById('root')
)
```

Avec une première props :
```
function CoolComponent({ adjective = 'Cool' }) {
  return <p>Youpi So {adjective} !</p>
}

ReactDOM.render(
  <div>
    <CoolComponent adjective="awesome" />
    <CoolComponent />
  </div>,
  document.getElementById('root')
)
```
Donnera :
<center>youpi so awesome !
<center> youpi so Cool !



__<span style="color:yellow">Avec JSX</span>__ :
  `<User first="John" last="Smith" />`


Sans JSX :
  `React.createElement(User, { first: 'John', last: 'Smith' })`

&nbsp;

Un __<span style="color:yellow">Form avec JSX</span>__ :
```
<form method="post" action="/sessions" onSubmit={this.handleSubmit}>
  <p className="field">
    <label>
      E-mail
      <input
        type="email"
        name="email"
        required
        autoFocus
        value={this.state.email}
        onChange={this.handleFieldChange}
      />
    </label>
  </p>
  <p className="field">
    <label>
      Mot de passe
      <input
        type="password"
        name="password"
        required
        value={this.state.password}
        onChange={this.handleFieldChange}
      />
    </label>
  </p>
  <p>
    <button type="submit" value="Connexion" />
  </p>
</form>
```  
le même form __sans JSX__ (moins lisible):
```
React.createElement(
  'form',
  { method: 'post', action: '/sessions', onSubmit: this.handleSubmit },
  React.createElement(
    'p',
    { className: 'field' },

    React.createElement(
      'label',
      null,
      'E-mail',

      React.createElement('input', {
        type: 'email',
        name: 'email',
        required: true,
        autoFocus: true,
        value: this.state.email,
        onChange: this.handleFieldChange,
      })
    )
  ),

  React.createElement(
    'p',
    { className: 'field' },

    React.createElement(
      'label',
      null,
      'Mot de passe',

      React.createElement('input', {
        type: 'password',
        name: 'password',
        required: true,
        value: this.state.password,
        onChange: this.handleFieldChange,
      })
    )
  ),

  React.createElement(
    'p',
    null,
    React.createElement('button', { type: 'submit', value: 'Connexion' })
  )
)
```
__Le code est ainsi beaucoup plus lisible avec JSX, on comprend mieux les interactions__

Attention __JSX__ est un language à balise __Case Sensitif__ qui exige de les fermer toutes et dans le bon ordre. On utilise du <span style="color:yellow">camelCase</span> avec première lettre en minuscule, puis chaque mot débute avec une Majuscule

### Valeur des props
En JSX __on ne parle pas d'attributs mais de Props__.
Une prop peut avoir nimporte quelle valeur JS, mais syntaxiquement c'est soit :
- un String avec des doubles ""
- une expression définie entre accolades {}
```
<input
  type="email"
  name="email"
  maxlength={42}
  readonly={false}
  onChange={this.handleFieldChange}
  value={this.state.value}
/>
```
Ici les props type et name ont des valeurs String (comme en html) __"name"__
Pour ce qui n'est pas de type string par contre il faut utiliser des expressions __{42}__


 Pour un type réel de type __true__, on se contente juste d'afficher la prop:
<input type="email" name="email" <span style="color:yellow">autoFocus required</span> />

### Mots réservés
Impossible d'utiliser pour autre chose les mots clés JS type <span style="color:yellow">className</span> (pour class) et chtmlFor</span> (pour for).

### Commentaires
Pas de syntaxe dédiée. `<!-- -->`  ou  `/*  */` fonctionnent.



# Exemple avec un jeu de "Memory"

Le jeu aura 2 composants métier :
## Le composant Card
Dans un fichier Card.js :
```
const Card = ({ card, feedback }) => (
  <div className={`card ${feedback}`}>
    <span className="symbol">
      {feedback === 'hidden' ? HIDDEN_SYMBOL : card}
    </span>
  </div>
)
```
- on déstructure les props passés en arguments <span style="color:yellow">card</span> et <span style="color:yellow">feedback</span>
- la fonction renvois directement une grappe DOM virtuel. on n'a donc pas de return et d'accolades {}
- pour le div on a une expression JSX ${} entre ` ```

## Le composant GuessCount qui compte les essais
Dans un fichier GuessCount.js :

`const GuessCount = ({ guesses }) => <div className="guesses">{guesses}</div>`



## Le composant Application (App.js)
```
import Card from './Card'
import GuessCount from './GuessCount'

class App extends Component {
  render() {
    return (
      <div className="memory">
        <GuessCount guesses={0} />
        <Card card="😀" feedback="hidden" />
        <Card card="🎉" feedback="justMatched" />
        <Card card="💖" feedback="justMismatched" />
        <Card card="🎩" feedback="visible" />
        <Card card="🐶" feedback="hidden" />
        <Card card="🐱" feedback="justMatched" />
      </div>
    )
  }
}
```
4 feedbacks sont prévus : visible, hidden, justMatched, justMismatched


# Les évènements
L'idée est de connecter nos gestionnaires d'évènements depuis le code JSX.

## La délégation d'évènements
On les attache __le plus au possible__ et on bénéficie de la __propagation__.
Mais des évènements de type submit, focus, change... ne respectent pas la __délégation d'évènements__.


### Exemples :
affichage dans le console.log sur un clic :
```
const Greeter = ({ whom }) => (
  <button onClick={() => console.log(`Bonjour ${whom} !`)}>
    Vas-y, clique !
  </button>
)

ReactDOM.render(<Greeter whom="Roberto" />, document.getElementById('root'))
```

Un boutton toggle :
```
class LogEntry extends Component {
  // …
  render() {
    const className = `log entry ${this.isOpen() ? 'open' : 'closed'}`
    return (
      <li className={className} onClick={this.toggle}>
        …
      </li>
    )
  }
}
```

## Il n'y a pas d'instructions, mais que des expressions
Il faut remplacer :  <span style="color:red">if, for</span> ... par : <span style="color:yellow">&&, ||</span> et l'opérande ternaire <span style="color:yellow">? :  </span>
Pour retranscrire un contenu conditionnel simple on utilisera __&&__ :

`<p>{42 > 43 && document.nonExistingMethod()}</p>`

Le paragraphe est vide car 42 est inférieur à 43 et retourne donc __false__.

&nbsp;

Si on initialise la propriété de admin à true :

`<p>{user.admin && <a href="/admin">Faire des trucs de VIP</a>}</p>`

La ligne s'affichera.


### if ... else
Avec l'opérande ternaire :
```
<p>{user.loggedIn ? <WelcomeLabel /> : <LoginLink />}</p>

<p>{user.admin ? (
  <a href="/admin">Faire des trucs de VIP</a>
) : (
  <a href="/request-admin">Demander à devenir VIP</a>
)}</p>
```



## Découpage et factorisation de notre JSX
Si un bouton existe à plusieurs endroits, on le factorise dans une fonction (ici logoutButton) :
```
render() {
  const logoutButton = (
    <button onClick={this.logOut}>
      <LogoutIcon />
      Déconnexion
    </button>
  )

  return (
    <Card>
      <CardTitle>
        Oh le joli titre
        {logoutButton}
      </CardTitle>
      …
      <Footer>
        © 2017 Des Gens Bien™ •
        {logoutButton}
      </Footer>
    </Card>
  )
}
```


## les boucles
on utilise la méthode __map__ pour produire le composant.
```
const numbers = [1, 2, 3, 4]
const doubles = numbers.map(x => x * 2) // [2, 4, 6, 8]
```
Si on a une liste de Users :
```
const users = [
  { id: 1, name: 'Alice' },
  { id: 2, name: 'Bob' },
  { id: 3, name: 'Claire' },
  { id: 4, name: 'David' },
]
```

et que l'on veut créer une liste de liens :

```
render () {
  return (
    <div className="userList">
      {this.props.users.map((user) => (
        <a href={`/users/${user.id}`}>{user.name}</a>
      ))}
    </div>
  )
}
```
et si on déstructure :
```
render () {
  return (
    <div className="userList">
      {this.props.users.map(({ id, name }) => (
        <a href={`/users/${id}`}>{name}</a>
      ))}
    </div>
  )
}
```
### Dans des listes, la prop: __key=__
Permet de garder une association correcte si jamais la liste change
```
render () {
  return (
    <div className="userList">
      {this.props.users.map(({ id, name }) => (
        <a href={`/users/${id}`} key={id}>{name}</a>
      ))}
    </div>
  )
}
```
