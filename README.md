# Travail réalisé sur le Projet React:
- Création du socle de l’application en 3 parties avec des composants:  **OK**
- BrowsePanel, EditMetaSlid, BrowseContent lisent les bons JSON : **OK**
- Manipulation des Reducers :
    - Mise à jour des slides et des infos dans la présentation : **OK**
    - Mise à jour de l’EditPanel : **OK**
- Création d’un service de communication **KO**
- Mise en place des évènements Drag’n’Drop: **OK**
    - Mise à jour d’une image par dragndrop sur le slide: **OK**
    - Mise à jour d’une vidéo ou d’une iframe sur le slide: **KO**
    - Section d’ajout: **KO** 
- Slide navigation, présentation, add slide, save: **KO**

## Rendus supplémentaire:
+ Vidéo de démo disponible : [DEMO_REACT_girard-duffoug-bancalero.mp4](https://gitlab.com/5irc_jee_nodejs_react/5irc_projet_react/blob/master/DEMO_REACT_girard-duffoug-bancalero.mp4)
+ Réponse aux questions du TP React StepByStep dans le fichier : [React_reponses_questions_projet.md](https://gitlab.com/5irc_jee_nodejs_react/5irc_projet_react/blob/master/React_reponses_questions_projet.md)




## Liens vers les Repository de versionning:
**Le groupe contenant les trois projets:**

[https://gitlab.com/5irc_jee_nodejs_react](https://gitlab.com/5irc_jee_nodejs_react)

**Les trois projets:**

[repo projet JEE: https://gitlab.com/5irc_jee_nodejs_react/5irc_jee](https://gitlab.com/5irc_jee_nodejs_react/5irc_jee)

[repo projet Nodes: https://gitlab.com/5irc_jee_nodejs_react/5irc_nodejs](https://gitlab.com/5irc_jee_nodejs_react/5irc_nodejs)

[repo projet React: https://gitlab.com/5irc_jee_nodejs_react/5irc_projet_react](https://gitlab.com/5irc_jee_nodejs_react/5irc_projet_react)



## Participants de 5IRC: 
yassin.duffoug[@]gmail.com (Yassin Duffoug)

girard.baptiste[@]hotmail.fr (Baptiste Girard)

ubiklain[@]gmail.com (Ludovic Bancalero)