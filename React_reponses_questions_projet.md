# <center>Réponses aux questions du projet React</center>

# 3.1 Création d'un socle
### 3.1.2. A quoi sert une telle organisation dans un projet front ?
Elle nous permet de séparer les différentes fonctions de l'application React pour la rendre modulable.
Cela facilite la maintenabilité et son évolutivité.

### 3.1.3. Lors de la création d’un composant où positionneriez-vous les css spécifiques à ce composant ?
Au plus près du composant pour faciliter son exportabilité.

### 3.2.1. Proposer un découpage en composants de ce projet
3 colonnes :
- celle de gauche possède une liste de présentations
- celle du centre (la plus large), affiche un élément sélectionné dans la liste de la première colonne ainsi que son éditeur (form + drag'n'drop).
- celle de droite décomposée en deux composants superposés :
  - un composant drag'n'drop pour uploader de nouvelles images dans la liste de Contents
  - la liste des images qui ont été uploadées.

  
# 4.2 Création des composants MainPanel, BrowseContentPanel
### 4.2.1. A quoi sert la extends React.Component ?
A declarer l'héritage de type composant de React
il existe deux types de composants :
- composant fonctionnel (il prennent en paramètre un objet appelé props et retourne du JSX)
- composant à état: elle possède un render retournant du JSX. Elle recoit des props et possède un état (state) représentant l'état intenrne du composant.

### 4.2.2. Que représente props ? à quoi sert-il ?
Ce sont les propriétés d'une classe. 
Elle peut les recevoir en paramètre et devra automatiquement les passer en argument au constructeur Component.
Les props sont en lecture seule dans le composant qui les recoit, c'est pour cela qu'il faut les passer aux state

### 4.2.3. Que représente state ? à quoi sert-il ?
C'est l'état interne d'un composant. 
Il est modifiable avec la fonction setState().

### 4.2.4. Quelle est la fonction principale de render() ?
Cette fonction est indispensable pour tout composant stateful. 
Elle permet l'affichage, c'est à dire le rendu de la vue dans un format HTML
Pour cela, si le State est modifié par un nouvel Objet, il met à jour le VirtualDOM qui lui même met à jour le DOM du navigateur

### 4.2.5. Modifier votre application react.js afin de prendre en compte ce composant
Dans le fichier racine App.js :   import './components/mainPanel/Main.js';

### 4.3.3. Pourquoi est-il intéressant de proposer des composants « visuels» ?
Cela permet de découper nos composants par type de contenu à afficher
On peux également décider de les afficher ou non selon certaines conditions de l'état du composant
 
### 4.3.4. Que représente les différents répertoires de components/common/Container (containers, components) ?
Un container peut regrouper plusieurs composants.
Ces composants sont ainsi modulables

### 4.3.5. Pourquoi le composant Container a-t-il été placé dans un répertoire common ?
Il est commun a toute l'application et ses contenus peuvent être réutilisable sur d'autres vues


# 5 Création des composants
### 5.3.1. Pourquoi le composant EditMetaSlide est-il considéré comme un composant visuel?
Car il devra pouvoir être affiché ou non selon son positionnement (dans BrowsePresentation ou dans l'editeur de slide)

### 5.3.2. A quoi correspond l’attribut onChange ? que se passe-t-il si cet attribut n’est pas setté ?
il permet d'appeler une fonction et ainsi le reducer lorsquel'on saisi du texte
si un nouvel objet n'est pas créé les valeurs ne seront jamais mise à jour via le Reducer vers ses subscribers

### 5.3.3. A quoi servent les fonctions this.props.handleChangeTxt et this.props.handleChangeTitle ? qui définit ces fonctions ?
A permettre de dispatcher la nouvelle valeur vers le reducer et ses subscribers (les présentations)



# 6 Manipulation des actions et des reducers
### 6.4.2 A quoi sert  connect()(Slid) ? et pourquoi n'y a t il pas d'argument dans connect()
A se connecter au reducer, il ne prend pas d'argument car on ne se souscrit à aucun reducer dans ce composant, on ne fait que dispatcher de l'info vers les autres composants

### 6.4.3 que fait la ligne this.props.dispatch(setSelectedSlid(Tmpslid) ?
A dispatcher le nouvel objet vers le reducer et ensuite ss subscribers

### 6.4.4 a quoi correspond setSelectedSlid() ?
A choisir le bon Reducer parmis tout ceux pouvant exister en un click dans le Browser de présentation

### 6.4.5 que se passerait-il si un nouvel objet n'est pas créé ?
Il n'y aurait aucun nouveau rendu dans le navigateurcar le virtual DOM ne serait pas mis à jour.


# 7  Création d'un service de communication
### 7.1.2 Que permet de réaliser la fonction loadPres(presId,callback,callbackErr) 
axios est un client léger HTTP qui, une fois le composant loadPres chargé va permettre d'aller charger de facon asynchrone une présentation
Si la présentation n'est pas vide (length > 0), on lance la fonction de callback qui executera la fonction de traitement de la présentation
Sinon, on lance la fonction de callback d'erreur qui avertit que le chargement n'a pas abouti


# 9 Navigation et diffusion des présentation
###  9.1.8 Que veut dire la fonction store.subscribe(( => {...})
A s'abonner au Store qui diffuse les présentations (depuis un autre serveur comme par exemple NodeJS)

### 9.2.7 Qu'est ce que socket.io
Une bibliotheque React de services permettant de communiquer avec des serveurs tiers. 
Elle permet d'ouvrir des sockets de communications bidirectionnels.