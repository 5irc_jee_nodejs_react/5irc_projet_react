import React, { Component } from 'react';
import './lib/bootstrap-3.3.7-dist/css/bootstrap.min.css';
import * as contentMapTmp from "./source/contentMap";
import * as presentations from "./source/pres";

import BrowseContent from './components/browseContentPanel/containers/BrowseContentPanel';
import BrowsePresentation from './components/browsePresentationPanel/containers/BrowsePresentationPanel';

//import Slid from './components/common/presentation/slid/containers/Slid';
import EditSlidPanel from './components/editSlidPanel/EditSlidPanel';

//import needed to use redux with react.js
import { createStore } from 'redux';
import myReducers from './reducers'
import { Provider } from 'react-redux';

//create a store and associate reducers to it
const store = createStore(myReducers);


class App extends Component {

    constructor(props) {
        super(props);
        let content_list;
        let pres_list;

        content_list=contentMapTmp.default.contents;
        pres_list=presentations.default;


        this.state = {
             contentMap:contentMapTmp.default,
             selected_content_id:0,
             content_list:content_list,
             pres_list:pres_list,
             selected_content:pres_list.slidArray[0]
        };
    }


    render() {
        return (
            <Provider store={store} >
                <div className='container-fluid height-100'>
                  <div className="row height-100">
                      <div className='col-md-3 col-lg-3 height-100'>
                          <BrowsePresentation
                              presentation={this.state.pres_list}
                              contents={this.state.content_list}
                              handleOnContentSelected={this.handleOnContentSelected}
                          />
                      </div>
                      <div className='col-md-6 col-lg-6 height-100'>
                          <EditSlidPanel
                              selected_slid={this.state.selected_content}
                              contentMap={this.state.content_list}
                              title={null}
                              txt={null}
                          />
                      </div>
                      <div className='col-md-3 col-lg-3 height-100 vertical-scroll'>
                          <BrowseContent
                              contents={this.state.content_list}
                          />
                      </div>
                  </div>
                </div>
            </Provider>
        );
    }
}

export default App;
