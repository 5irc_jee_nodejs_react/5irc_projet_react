// Action permettant de sélectionner la présentation à éditer
export const setSelectedSlid=(slid_obj)=>{
    return {
        type: 'UPDATE_SELECTED_SLID',
        obj:slid_obj
    };
}

// Action mettant à jour la Présentation courante
export const updatePresentation = (presentation) => {
    return {
        key: presentation.id,
        type: 'UPDATE_PRESENTATION',
        presentation:presentation,
    };
}
