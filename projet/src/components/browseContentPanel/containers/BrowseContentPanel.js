import React, { Component } from 'react';
import Content from '../../common/content/Content';

class BrowseContentPanel extends Component {
    //class constructor whith given properties
    constructor(props) {
        super(props);
        this.getAllContents=this.getAllContents.bind(this);
    }

    //Récupère tous les contenus
    getAllContents(){
        let array_render=[];
        for(var i=0;i<this.props.contents.length;i++){
            array_render.push(
                <Content
                    key={i}
                    content={this.props.contents[i]}
                />
            );
        }
        return array_render;
    }

    //render function use to update the virtual dom
    render() {
        const display_list= this.getAllContents();
        return (
            <div  className='overflow-scrolling'>
                {display_list}
            </div>
        );
    }
}

//export the current classes in order to be used outside
export default BrowseContentPanel;
