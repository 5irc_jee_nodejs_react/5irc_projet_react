import React, { Component } from 'react';
import Presentation from '../../common/presentation/containers/Presentation';
import { connect } from 'react-redux';
import './browsePresentationPanel.css'

/**
 * Cette classe représente le Browser de Présentations
 * Il contient :
 *    - Les présentations qui contiennent chacune:
 *    - les infos de titres + textes
 *    - le Slide
 *       - le Slide contient un content (img, iframe, video..)
 *          - composé de labels et du visuel
 */
class BrowsePresentationPanel extends Component {
    //class constructor with given properties
    constructor(props) {
        super(props);
        this.state = {
            title:this.props.title,
            txt:this.props.txt,
            id:this.props.id,
            content_id:this.props.content_id
        };

    }

    //render function use to update the virtual dom
    render() {


        return (
            <div >
                <div>{ this.props.presentation.title }</div>
                <div>{ this.props.presentation.description }</div>
                <div className='overflow-scrolling'>
                    <Presentation
                        slides={ this.props.presentation.slidArray }
                        contents={ this.props.contents }
                        displayMod={"PRES"}
                    />
                </div>
            </div>
        );
    }
}

export default connect() (BrowsePresentationPanel);
