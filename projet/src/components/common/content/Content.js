import React, { Component } from 'react';
import '../../../lib/bootstrap-3.3.7-dist/css/bootstrap.min.css';
import Label from './containers/Label.js';
import Visual from './containers/Visual.js';

/**
 * Cette classe représente le composant Content
 * Il contient deux composants :
 *    - Les labels du content
 *    - Le visuel du content
 */
class Content extends Component {

    render() {
        return (
            <div className="panel panel-default">
                <Visual
                    className="visual"
                    type={this.props.content.type}
                    alt={this.props.content.title}
                    src={this.props.content.src}
                />
                <Label
                    title={this.props.content.title}
                    id={this.props.content.id}
                />
            </div>
        );
    }
}

export default Content;