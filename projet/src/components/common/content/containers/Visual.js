import React, { Component } from 'react';
import Iframe from 'react-iframe';

class Visual extends Component {

    onDragEnter = (e) => {
        let event = e;
        event.stopPropagation(e);
    }

    render() {
      let render_visual;

      switch(this.props.type){
        case "img_url":
            render_visual=(
                <img
                    id={"drag1"}
                    draggable={"true"}
                    onDragStart={this.onDragEnter}
                    className='imgCard'
                    alt='imgCard'
                    src={this.props.src}
                />
                );
        break;

        case "video":
              render_visual=(
               <div className="video">
                   <object
                      id={"object"}
                      draggable={"true"}
                      onDragStart={this.onDragEnter}
                      width="100%" height="100%"
                      data={this.props.src}>
                    </object>
               </div>
                );
        break;

        case "web":
          render_visual=(
              <div >
                  <div className="video"
                      id={"drag1"}
                      draggable={"true"}
                      onDragStart={this.onDragEnter}
                  >
                      <Iframe url={this.props.src}
                              id={"iframe"}
                              draggable={"true"}
                              onDragStart={this.onDragEnter}
                              className="myClassname"
                              display="initial"
                              position="relative"
                              allowFullScreen/>
                  </div>
              </div>
          );
          break;

        default:
          render_visual=(
              <h3>Aucun contenu à afficher</h3>
          )
        }



      return (
            <div className="thumbnail">
                {render_visual}
            </div>            
    );
  }
}

export default Visual;