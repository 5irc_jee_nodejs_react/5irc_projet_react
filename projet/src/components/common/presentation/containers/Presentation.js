import React, { Component } from 'react';
import '../../../../lib/bootstrap-3.3.7-dist/css/bootstrap.min.css';
import Slid from '../slid/containers/Slid';
import { connect } from 'react-redux';

/**
 * Classe représentant un composant de type Présentation
 *    il contient:
 *    - les infos de titres + textes
 *    - le Slide
 *       - le Slide contient un content (img, iframe, video..)
 *          - composé de labels et du visuel
 */
class Presentation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title:this.props.title,
            txt:this.props.txt,
            id:this.props.id,
            content_id:this.props.content_id
        };
        this.getAllContents=this.getAllContents.bind(this);
    }

    getAllContents(){
        const contentMap = this.props.contents;
        return this.props.slides.map((slide) =>
            <Slid
                key={ slide.id }
                slid={ slide }
                contents={ contentMap }
                displayMod = "FULL_MNG"
            ></Slid>
        );
    }


    render() {
        const display_list= this.getAllContents();
        return (
            <div className="browsediv">
                <div className="panel panel-default">
                    {display_list}
                </div>
            </div>
        );
    }
}


export default connect() (Presentation);