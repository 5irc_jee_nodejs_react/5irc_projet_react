import React from 'react';
import './editMetalSlid.css'
import {connect } from 'react-redux';
import { updatePresentation } from '../../../../../actions/index'

/**
 * Classe permettant la mise à jour des présentations
 * Elle contient :
 *    - Le slide à modifier
 *    - Les inputs de modification des titres et textes de la présentation
 *    - Une zone de Drag'n'Drop pour mettre à jour l'image de la présentation
 */
class EditMetaSlid extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
                        title:this.props.title,
                        txt:this.props.txt,
                        id:this.props.id,
                        content_id:this.props.content_id
                        };
        this.handleChange=this.handleChange.bind(this);
    }


    // Dispatch les modifications des présentations
    handleChange(e) {
        if (e.target.id === 'currentSlideTitle') {
            this.state.title = e.target.value; //mutate state directly to have in realTime modification
        }
        if (e.target.id === 'currentSlideTxt') {
            this.state.txt = e.target.value; //mutate state directly to have in realTime modification
        }

        // le nouvel objet pour permettre la mise à jour du VirtualDOM
        const newPres = {
            title: this.state.title,
            txt: this.state.txt,
            id: this.state.id,
            content_id: this.state.content_id
        };

        this.props.dispatch(updatePresentation(newPres));
    }

    // Drag'n'drop handlers
    onDragOver = (e) => {
        let event = e;
        event.stopPropagation();
        event.preventDefault();
    };

    onDragEnter = (e) => {
        let event = e;
        event.stopPropagation();
    };

    // Drop IMG dispatch vers le reducer la mise à jour du content de la présentation
    onFileDrop = (e) => {
        let event = e;
        event.stopPropagation();

        var data = e.dataTransfer.getData("url");
        // nouvel objet pour permettre la mise à jour du virtual DOM
        const newPres = {
            title: this.state.title,
            txt: this.state.txt,
            id: this.state.id,
            content_id: this.state.content_id,
            newImg: data
        };

        //Dispatch vers le reducer et les subscribers
        this.props.dispatch(updatePresentation(newPres));
    };


    render(){
        return (
            <div className="form-group" key={this.props.id}>
                <div  onChange={this.handleChange}>
                    <label htmlFor="currentSlideTitle">Title </label>
                    <input key={this.props.title}
                           type="text"
                           className="form-control"
                           id="currentSlideTitle"
                           onChange={this.handleChange}
                           defaultValue={this.props.title || ''}
                    />
                </div>
                <div>
                    <label htmlFor="currentSlideTxt">Text</label>
                    <input key={this.props.txt}
                           type="text"
                           className="form-control"
                           id="currentSlideTxt"
                           onChange={this.handleChange}
                           defaultValue={this.props.txt || ''}
                    />
                </div>
                <div
                    className="dnd"
                    onDragEnter={this.onDragEnter}
                    onDragOver={this.onDragOver}
                    onDrop={this.onFileDrop}
                    style={{display: 'flex',
                            justifyContent:'center',
                            alignItems:'center',
                            height: '20vh'}}
                >
                    Drag and drop content image here to replace actual content
                </div>
            </div>
        );
    }
}

export default connect()(EditMetaSlid);
