import React, { Component } from 'react';
import '../../../../../lib/bootstrap-3.3.7-dist/css/bootstrap.min.css';
import Content from '../../../content/Content';
import './slid.css';
import { connect } from 'react-redux';
import {setSelectedSlid} from '../../../../../actions/index'

/**
 *  Cette gère le composant de Slide
 *  il contient :
 *      - Les contents composés de :
 *         - Des labels
 *         - Un visuel
 *  Il est subscriber à l'UpdateModelReducer pour:
 *     - Mettre à jour les labels (titres, textes...)
 *     - Mettre à jour l'image du Slide et de sa présentation
 */
class Slid extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title:this.props.slid.title,
            txt:this.props.slid.txt,
            id:this.props.slid.id,
            content_id:this.props.slid.content_id,
            newImg:this.props.slid.src,
            contentToDisplay:null
        };

        this.getContent = this.getContent.bind(this);
        this.updateSelectedSlid=this.updateSelectedSlid.bind(this);
    }

    getTitle()
    {

        if (this.props.displayMod === 'NOTITLE') {
            return (<div></div>);
        }
        return(
            <div
                key={this.props.idToModify}
                id="div1"
            >
                <label >Title</label>
                <div className="title">{this.state.title}</div>
                <label >Txt</label>
                <div className="title">{this.state.txt}</div>
            </div>
        );
    }


    // Récupération du Content à afficher dans le slide sélectionné
    getContent() {
        // update de la presentation selon les modifs provenant du reducers d'editSlidPanel
        var title = this.state.title;
        var txt = this.state.txt;

        //on ne modifie que le slide sélectionné
        if(this.state.id === this.props.idToModify) {
            this.state.title = this.props.titleToModify; //mutate state directly to have in realTime modification
            this.state.txt = this.props.txtToModify; //mutate state directly to have in realTime modification
            if(this.props.newImg !== undefined && this.props.newImg !== '' ) {
                this.state.contentToDisplay.src = this.props.newImg; //mutate state directly to have in realTime modification
                this.state.contentToDisplay.type = "img_url"; //mutate state directly to have in realTime modification
            }
        }
        this.state.contentToDisplay = this.props.contents.find(
            (content) => content.id === this.props.slid.content_id,
        );

        const divtitle = this.getTitle()
        return (
            <div>
                { divtitle }
                <Content
                    content={ this.state.contentToDisplay }
                />

            </div>

        )
    }

    // Dispatch au reducer du slid à Editer dans le corps central de l'apps
    updateSelectedSlid(){
        const tmpSlid={
                        displayMod:'EDITION',
                        id:this.state.id,
                        title:this.state.title,
                        txt:this.state.txt,
                        content_id:this.state.content_id,
                        newImg: this.state.content_id};

        this.props.dispatch(setSelectedSlid(tmpSlid));
    }

    render() {
        return (
            <div className="panel panel-default" onClick={this.updateSelectedSlid}>
                <div className="panel-body">
                    { this.getContent() }
                </div>
             </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {


    let newImg = '';
    if(state.updateModelReducer.newImg !== undefined){
        newImg = state.updateModelReducer.newImg;
    }

    return {
        titleToModify : state.updateModelReducer.title,
        txtToModify : state.updateModelReducer.txt,
        idToModify : state.updateModelReducer.id,
        newImg : newImg
    }
};

export default connect(mapStateToProps)(Slid);