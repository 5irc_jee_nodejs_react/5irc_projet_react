import React from 'react';
import {connect } from 'react-redux';
import Slid from '../common/presentation/slid/containers/Slid'
import EditMetalSlid from "../common/presentation/slid/components/EditMetalSlid";

/**
 * Cette classe gère le composant central de l'appli : l'éditeur de présentation
 * Il contient :
 *    - Un slide
 *    - Un EditMetalSlide
 * Il est subscriber au Reducers selectedReducer pour se mettre à jour
 * selon la présentation sélectionnée dans la liste gauche BrowsePresentations
 */
class EditSlidPanel extends React.Component {
    render() {
        if(    this.props.selected_slid.title===undefined){
            return (<div style={{display: 'flex',  justifyContent:'center', alignItems:'center', height: '100vh'}}>
                        Merci de sélectioner un slide
                    </div>);
        }
         return (
                <div>
                    <Slid
                        className="slide"
                        slid = {this.props.selected_slid}
                        contents = {this.props.contentMap}
                        displayMod = "NOTITLE"
                    />
                    <EditMetalSlid key={this.props.selected_slid.id}
                                   title={this.props.selected_slid.title}
                                   txt={this.props.selected_slid.txt}
                                   id={this.props.selected_slid.id}
                                   content_id={this.props.selected_slid.content_id}
                    />
                </div>


        );

    }

}

//Mapping entre le Reducer et les state de la présentation à éditer
const mapStateToProps = (state, ownProps) => {
    return {
        selected_slid: state.selectedReducer.slid,
    }
};
//connection au Reducer
export default connect(mapStateToProps)(EditSlidPanel);