// Reducers permettant de diffuser la mise à jour de la présentation à sélectionner aux subscribers
const selectedReducer= (state={slid:{}},action) => {
    switch (action.type) {
        case 'UPDATE_SELECTED_SLID':
            const newState1={
                slid:action.obj};
            return newState1;
        default:
            return state;
    }
};
export default selectedReducer;