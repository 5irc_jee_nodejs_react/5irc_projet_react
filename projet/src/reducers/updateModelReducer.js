// Reducers permettant de mettre à jour la présentation aux subscribers (la liste des présentations)
const updateModelReducer = (
    state = {
        presentation: null,
    },
    action
    ) => {
    switch (action.type) {
            case 'UPDATE_PRESENTATION':
                const newState = state;
                newState.presentation = action.presentation;
                return newState.presentation;
        default:
            return state;
    }
}

export default updateModelReducer;